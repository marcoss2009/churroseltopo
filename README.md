# Churros El Topo

Proyecto de clase de Churros El Topo.

Ir agregando contenido relevante al proyecto modificando este archivo con el formato MD con [StackEdit](https://stackedit.io).

# Lista de *branches*

Cada miembro tiene asignado un *branch*, tenerlo en cuenta de ahora en adelante.

 - ***menu***: Ezequiel
 - ***usuarios***: Ruben
 - ***gastos***: Marcos
 - ***ventas***: Matias
 - ***historial***: Carla
 - ***status***: Ana

# Lista de Comandos

Lista de comandos git dependiendo de lo que se necesite hacer.

## Primer paso, posicionarme en mi *branch*
Crear una carpeta vacía donde se va a trabajar en el proyecto, e inicializar tu branch con los siguientes comandos.

    git init
    git remote add origin git@gitlab.com:marcoss2009/churroseltopo.git
    git fetch
    git checkout -t origin/TU BRANCH
    

## Subir actualizaciones
Para subir actualizaciones posicionarse en la carpeta donde estas trabajando y ejecutar los siguientes comandos.

    git add .
    git commit -am "Descripción corta de la actualización"
    git push -u origin TU BRANCH
    
## Descargar actualizaciones del branch *master*

Les recomiendo que para descargar el proyecto principal (del branch *master*) lo hagan en una carpeta aparte y ejecutar los siguientes comandos **POR UNICA VEZ**.

    git init
    git remote add origin git@gitlab.com:marcoss2009/churroseltopo.git
    git fetch
    git checkout -t origin/master
    
**PARA DESCARGAR NUEVAS ACTUALIZACIONES UTILIZAR LOS SIGUIENTES COMANDOS**

    git fetch origin
    git reset --hard origin/master

 # Estructura de carpetas
 Dentro de cada carpeta (Db, Forms, FyP, Img) crear una carpeta con el nombre del tu módulo y trabajar **SIEMPRE** desde esa carpeta.
